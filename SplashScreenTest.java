import java.awt.*;
import javax.swing.*;
public class SplashScreenTest {
	public static void constructGUI() {
		SplashScreen splash = SplashScreen.getSplashScreen();
		System.out.println(splash == null);
		if (splash != null) {
			Graphics2D g = (Graphics2D) splash.createGraphics();
			
			for (int i = 0; i < 10; i++) {
				String message = "Process " + i + " of 10 ...";
				g.setComposite(AlphaComposite.Clear);
				g.fillRect(130, 350, 280, 40);
				g.setPaintMode();
				g.setColor(Color.RED);
				g.drawString(message, 130, 360);
				g.fillRect(130, 370, i * 30, 20);
				splash.update();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println(e);
				}
			}
			
			
		}
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(3);
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				constructGUI();
			}
		}
		);
	}
}
