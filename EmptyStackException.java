/**
 * Exception for removing and getting element in an empty stack
 * @author dotoan
 *
 */
public class EmptyStackException extends RuntimeException {
	private static final long serialVersionUID = 244490188855298564L;
/**	
 * 
 * @param err
 */
	public EmptyStackException(String err) {
		//super(err);
	} 
}
