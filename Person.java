public class Person {
	int age;
	String name;
	public native int getAge();
	public native String getName();
	public native void setName(String name);
	public native void setAge(int age);
	static {
		System.loadLibrary("person");
	}
}