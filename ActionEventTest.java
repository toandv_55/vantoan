import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;

public class ActionEventTest extends JFrame implements ActionListener {
	public ActionEventTest() {
		super();
		init();
	}
	public static int count = 0;
	private void init() {
		this.setDefaultCloseOperation(3);
		this.setLayout(new FlowLayout());
		JButton button = new JButton("<html><body><h1>Click</h1></body></html>");
		button.addActionListener(this);
		this.add(button);
		this.pack();
		this.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		JButton button = (JButton) e.getSource();
		String text = button.getText();
		JOptionPane.showMessageDialog(null, text);
	}
	public static void constructGUI() {
		if (count++ < 2) {
			new ActionEventTest();
		}
	}
	public class Run implements Runnable {
		public void run() {
			constructGUI();
		}
	}
	public static void main(String[] args) {
		ActionEventTest test = new ActionEventTest();
		//System.out.println(count);
		Run run = test.new Run();
		run.run();
		System.out.println(count);
		Run run1 = test.new Run();
		System.out.println(count);
		Run run2 = test.new Run();
		System.out.println(count);
		
	}
}