#include <jni.h>
#include <stdio.h>
#include "Person.h"
JNIEXPORT jint JNICALL Java_Person_getAge(JNIEnv *env, jobject thisobj) {
	return thisobj->age;
}

JNIEXPORT jstring JNICALL Java_Person_getName
  (JNIEnv *env, jobject thisobj) {
	return thisobj->name;
  }
  
  JNIEXPORT void JNICALL Java_Person_setAge(JNIEnv *env, jobject thisobj, jint a) {
	thisobj->age = a;
  }
  
  JNIEXPORT void JNICALL Java_Person_setName(JNIEnv *env, jobject thisobj, jstring n) {
	thisobj->name = n;
  }