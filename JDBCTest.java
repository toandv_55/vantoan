import java.sql.*;

public class JDBCTest {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		}
		try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/", "root", "123")) {
			System.out.println(connection);
			Statement statement = connection.createStatement();
			statement.executeUpdate("create database if not exists dovantoan");
			statement.executeUpdate("use dovantoan");
			statement.executeUpdate("create table if not exists student(id int, name varchar(10))");
			//statement.executeUpdate("insert into student values('01', 'toan'),('02', 'kien')");
			ResultSet resultSet = statement.executeQuery("select * from student");
			System.out.println("ID    | Name");
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				System.out.println(id + "     | " + name);
				System.out.println("===========");
			}
		} catch (SQLException e) {
			System.out.println(e);
		}
	}
}