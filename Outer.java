public class Outer {
	String name = " van";
	public Logger getLogger(final String prefix) {
		class LoggerImp implements Logger {
			public void log(String log) {
				System.out.println( prefix + " : " + log + name);
			}
		}
		return new LoggerImp();
	}
	public static void main(String[] args) {
		Logger log = (new Outer()).getLogger("do");
		log.log("toan");
	}
}