﻿import java.io.InputStream;
import java.io.OutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
public class SerialTest {
	public static void main(String[] args) {
		Path path = Paths.get("D:\\work\\object_serialization");
		Student st = new Student("Do Toan");
		try (OutputStream os = Files.newOutputStream(
					path, StandardOpenOption.WRITE, 
							StandardOpenOption.CREATE);
				ObjectOutputStream objectOutputStream = new ObjectOutputStream(os)) {
					objectOutputStream.writeObject(st);
				
		} catch (IOException e) {
			System.out.println(e);
		}
		
		try (InputStream is = Files.newInputStream(path, StandardOpenOption.READ);
				ObjectInputStream objectInputStream = new ObjectInputStream(is)) {		
				Student student = (Student) objectInputStream.readObject();
				System.out.println(student.name);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}